//
// Created by Alex on 28.12.2021.
//

#ifndef LAB_PL_1_FILEWORK_H
#define LAB_PL_1_FILEWORK_H

#include <stdbool.h>
#include <stdio.h>

/*  deserializer   */
enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_PADDING_ERROR,
    READ_ERROR
    /* коды других ошибок  */
};

/*  serializer   */
enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_PADDING_ERROR,
    WRITE_PIXEL_ERROR
    /* коды других ошибок
     * файл закрыт
     * */
};


enum open_file{
    OPEN_OK = 0,
    OPEN_INCORRECT_NAME,
    OPEN_FAILD
};

enum close_file{
    CLOSE_OK = 0,
    CLOSE_FAILD
};

enum open_file open_f (FILE** f, const char* name, const char* mode);

enum close_file close_f (FILE* f);

//функция для открытия и закрытия файлов,
//на открытых файлах могут сразу запускать from_bmp//to_bmp
//добавить обработку ошибок открытия/закрытия файла


#endif //LAB_PL_1_FILEWORK_H
