//
// Created by Alex on 28.12.2021.
//

#pragma once
#ifndef LAB_PL_1_IMAGE_H
#define LAB_PL_1_IMAGE_H

#include <stdint.h>


struct pixel {
    uint8_t b, g, r;
};


struct image {
    uint64_t width, height;
    struct pixel* data;
};

//create
struct image img_create(uint64_t width, uint64_t height);

//free
void img_destroy( struct image* img );


#endif //LAB_PL_1_IMAGE_H
