//
// Created by Alex on 28.12.2021.
//

#ifndef LAB_PL_1_ROTATE_H
#define LAB_PL_1_ROTATE_H

#include "image.h"

/* создаёт копию изображения, которая повёрнута на 90 градусов */
struct image rotate( struct image const source );


#endif //LAB_PL_1_ROTATE_H

