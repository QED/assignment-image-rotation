//
// Created by Alex on 28.12.2021.
//

#ifndef LAB_PL_1_UTIL_H
#define LAB_PL_1_UTIL_H


#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

void err( const char* msg, ... ) __attribute__ ((noreturn));

#endif //LAB_PL_1_UTIL_H
