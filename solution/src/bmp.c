//
// Created by Alex on 28.12.2021.
//

#include "../include/bmp.h"
#include <inttypes.h>
#include <stdio.h>

#define BMP_BF_TYPE 0x4D42
#define BMP_BF_RESERVED 0
#define BMP_BI_SIZE 40
#define BMP_BI_PLANES 1
#define BMP_BI_COUNT 24
#define BMP_BI_COMPRESSION 0
#define BMP_BI_XPERMETER 0
#define BMP_BI_YPERMETER 0
#define BMP_BI_CLRUSED 0
#define BMP_BI_CLRIMP 0

static uint64_t padding (struct image const* img){
    return (4 - (img->width * 3 % 4)); //width = 9->padding = 3
}

static enum read_status bmp_header_save ( FILE* in, struct bmp_header* bmp ){
    const size_t read = fread( bmp, sizeof(struct bmp_header), 1, in);
    if ( read != 1 ){
        return READ_INVALID_HEADER;
    }
    return READ_OK;
}

static enum read_status read_pixel ( FILE* in, struct image const* img ){
    for( uint64_t i = 0; i < img->height; i++){
        for ( uint64_t j = i * img->width; j < img->width * ( i+1 ); j++){
            const size_t read = fread( &(img->data[j]), sizeof(struct pixel), 1, in); // нужно сместить поток in на padding
            if( read != 1 ) return READ_INVALID_BITS;                   // чтоб он не читал мусорные байты
        }
        if( fseek( in, padding( img ), SEEK_CUR ) != 0 ) return READ_PADDING_ERROR;
    }
    return READ_OK;
}

static struct bmp_header bmp_init(){
    return (struct bmp_header) {0};
}

enum read_status from_bmp ( FILE* in, struct image* img){
    struct bmp_header bmp = bmp_init();
    const enum read_status code = bmp_header_save( in, &bmp );
    if( code != READ_OK ) return code;
    *img = img_create(bmp.biWidth, bmp.biHeight);
    img->height = bmp.biHeight;
    img->width = bmp.biWidth;
    enum read_status code_pixel = read_pixel( in, img );
    if( code_pixel != READ_OK ) return code;
    return READ_OK;
}

static enum write_status bmp_header_write( FILE* out, struct image const* img, struct bmp_header* bmp){
    bmp->bfType = BMP_BF_TYPE;
    bmp->bfileSize = sizeof(struct bmp_header) + (sizeof(struct pixel) * img->height * img->width);
    bmp->bOffBits = sizeof(struct bmp_header);
    bmp->bfReserved = BMP_BF_RESERVED;
    //
    bmp->biSize = BMP_BI_SIZE;
    bmp->biWidth = img->width;
    bmp->biHeight = img->height;
    bmp->biPlanes = BMP_BI_PLANES;
    bmp->biBitCount = BMP_BI_COUNT;
    bmp->biCompression = BMP_BI_COMPRESSION;
    bmp->biSizeImage = sizeof(struct pixel) * img->width * img->height;
    bmp->biXPelsPerMeter = BMP_BI_XPERMETER;
    bmp->biYPelsPerMeter = BMP_BI_YPERMETER;
    bmp->biClrUsed = BMP_BI_CLRUSED;
    bmp->biClrImportant = BMP_BI_CLRIMP;

    const size_t write_code = fwrite(bmp, sizeof(struct bmp_header), 1, out);
    if( write_code != 1 ) return WRITE_ERROR;
    return WRITE_OK;
}

static enum write_status bmp_pixel_write( FILE* out, struct image const* img ){
    uint32_t const padd = 0;
    const uint8_t padding_count = padding( img );
    for( uint64_t i = 0; i < img->height; i++ ){
        for( uint64_t j = i * img->width; j < img->width * (i + 1); j++ ){
            const size_t code = fwrite(&(img->data[j]), sizeof(struct pixel), 1, out);
            if( code != 1 ) return WRITE_PIXEL_ERROR;
        }
        const size_t code_2 = fwrite(&padd, padding_count, 1, out);
        if( code_2 != 1 ) return WRITE_PADDING_ERROR;
    }
    return WRITE_OK;
}

enum write_status to_bmp ( FILE* out, struct image const* img ){
    struct bmp_header bmp = bmp_init();
    enum write_status header_code = bmp_header_write( out, img, &bmp );
    if( header_code != WRITE_OK ) return header_code;
    enum write_status pixel_code = bmp_pixel_write( out, img );
    if( pixel_code != WRITE_OK ) return pixel_code;
    return WRITE_OK;
}


