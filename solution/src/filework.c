//
// Created by Alex on 28.12.2021.
//

#include "../include/filework.h"

enum open_file open_f (FILE** f, const char* name, const char* mode){
    if( name == NULL ) return OPEN_INCORRECT_NAME;
    *f = fopen(name, mode);
    if( *f == NULL ) return OPEN_FAILD;
    return OPEN_OK;
}

enum close_file close_f (FILE* f){
    if( fclose(f) == 0 ) return CLOSE_OK;
    if( fclose(f) == EOF ) return CLOSE_FAILD;
    return CLOSE_OK;
}

