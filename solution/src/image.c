//
// Created by Alex on 28.12.2021.
//
#include "../include/image.h"
#include <malloc.h>

struct image img_create(uint64_t width, uint64_t height){
    struct image img;
    img.width = width;
    img.height = height;
    img.data = malloc( sizeof(struct pixel) * width * height );
    return img;
}

void img_destroy( struct image* img ){
    free(img->data);
    //img->data = NULL;
    //img->width = 0;
    //img->height = 0;
}
