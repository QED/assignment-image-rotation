#include "../include/bmp.h"
#include "../include/rotate.h"
#include "../include/util.h"
#include <stdio.h>


int main( int argc, char** argv ) {
    //(void) argc; (void) argv; // supress 'unused parameters' warning
    if( argc != 3 ){
        err("Not enough argc");
    }

    struct image img;
    FILE* in = NULL;
    enum open_file open_code = open_f(&in, argv[1], "rb");
    if( open_code != OPEN_OK){
        err("open error: %d", argv[1], open_code);
    }
    enum read_status from_bmp_code = from_bmp(in, &img);
    if( from_bmp_code != READ_OK ){
        err("read error: %d", argv[1], from_bmp_code);
    }
    enum close_file close_code = close_f(in);
    if( close_code != CLOSE_OK){
        err("close error: %d", argv[1], close_code);
    }

    struct image new_img = rotate( img );

    FILE* out = NULL;
    enum open_file open_code_new = open_f(&out, argv[2], "wb");
    if( open_code_new != OPEN_OK){
        err("open error: %d", argv[2], open_code_new);
    }
    enum write_status write_img_code = to_bmp(out, &new_img);
    if( write_img_code != WRITE_OK ){
        close_f(out);
        err("write error: %d", argv[2], write_img_code);
    }
    enum close_file close_code_new = close_f(out);
    if( close_code_new != CLOSE_OK ){
        err("close error: %d", argv[2], close_code_new);
    }

    img_destroy(&img);
    img_destroy(&new_img);
    return 0;
}
