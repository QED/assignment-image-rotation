//
// Created by Alex on 28.12.2021.
//

#include "../include/image.h"
#include "../include/rotate.h"

#include <stdint.h>

static struct pixel coordinates ( struct image const* source, uint64_t const it, uint64_t const sub ){
    return source->data[source->width * it - sub];
}

struct image rotate( struct image const source){
    struct image img;
    img = img_create(source.height, source.width);
    uint64_t sub = source.width;
    for( uint64_t i = 0; i < source.width; i++){
        uint64_t it = source.height;
        for( uint64_t j = i * img.width; j < source.height * (i + 1); j++){
            img.data[j] = coordinates(&source, it, sub);
            it--;
        }
        sub--;
    }
    return img;
}

